from checksumdir import dirhash
from hashlib import sha256
from time import sleep


def get_hash(my_list):
    my_h = sha256()
    for i in my_list:
        my_h.update(dirhash(i).encode('utf-8'))
    return my_h.hexdigest()

path_list = [
    'D:\\fac_repos\\myrfa\\Server',
    'D:\\fac_repos\\myrfa\\Scripts',
    'D:\\fac_repos\\myrfa\\www',
]

my_dir_hash = ''

while True:
    local_hash = get_hash(path_list)
    if local_hash != my_dir_hash:
        print(True)

    my_dir_hash = local_hash
    sleep(2)
