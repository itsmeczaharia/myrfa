from DBManager.db_connector import DBSession
from Models.ThreadModel import ThreadModel
from myrfa.Models.UserModel import UserModel
from myrfa.Models.TopicModel import TopicModel


class ReMan:
    def __init__(self):
        self.db_session = DBSession()
        self.models = {
            'threads': ThreadModel(db_session=self.db_session),
            'users': UserModel(db_session=self.db_session),
            'topics': TopicModel(db_session=self.db_session),
        }

    def get_me_goodies(self, path, method='GET', payload=None):
        all_resources = self.models.keys()
        resource_name = ''
        attrs = {}
        result = None

        path = path.strip('/')

        if method in ['POST', 'PUT', 'DELETE']:
            path = path + '?' + payload

        if '?' in path:
            resource_name = path.split('?')[0]
            attrs = path.split('?')[1]

            attrs = attrs.split('&')
            attrs = {a.split('=')[0]: a.split('=')[1] for a in attrs}
        else:
            resource_name = path.strip('/')

        if resource_name not in all_resources:
            raise Exception(404, 'That %s Resource doesn\'t exist' % resource_name)

        try:
            if method is 'GET':
                result = self.models[resource_name].get_resource(**attrs)
            elif method is 'POST':
                result = self.models[resource_name].create_resource(**attrs)
            elif method is 'DELETE':
                result = self.models[resource_name].delete_resource(**attrs)
            elif method is 'PUT':
                result = self.models[resource_name].update_resource(**attrs)
        except Exception as e:
            raise e

        return result


if __name__ == '__main__':
    # reman = ReMan()

    pass