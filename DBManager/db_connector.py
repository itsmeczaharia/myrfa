from pymongo import MongoClient


class DBSession:
    DATABASE_NAME = 'Myrfa'
    USERS_COLLECTION_NAME = 'users'
    THREADS_COLLECTION_NAME = 'threads'
    TOPICS_COLLECTION_NAME = 'topics'
    POSTS_COLLECTION_NAME = 'posts'

    def __init__(self):
        self.client = MongoClient()
        self.db = self.client[self.DATABASE_NAME]
        self.threads = self.db[self.THREADS_COLLECTION_NAME]
        self.topics = self.db[self.TOPICS_COLLECTION_NAME]
        self.posts = self.db[self.POSTS_COLLECTION_NAME]
        self.users = self.db[self.USERS_COLLECTION_NAME]
