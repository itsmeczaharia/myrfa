from pymongo import MongoClient
from bson.objectid import ObjectId

import urllib.request
from random import randint
import json
import re


class MyrfaDBCreator:
    DATABASE_NAME = 'Myrfa'
    USERS_COLLECTION_NAME = 'users'
    THREADS_COLLECTION_NAME = 'threads'
    TOPICS_COLLECTION_NAME = 'topics'
    POSTS_COLLECTION_NAME = 'posts'

    def __init__(self):
        self.client = MongoClient()
        self.db = self.client[self.DATABASE_NAME]
        self.threads = self.db[self.THREADS_COLLECTION_NAME]
        self.topics = self.db[self.TOPICS_COLLECTION_NAME]
        self.posts = self.db[self.POSTS_COLLECTION_NAME]
        self.users = self.db[self.USERS_COLLECTION_NAME]

    def create_users_collection(self):
        fd = open('dumps/users.dump')
        content = fd.read()
        self.users.insert_many(json.loads(content))


    def generate_topics(self):
        science_news = [
            'https://www.sciencenews.org/feeds/headlines.rss',
            'https://www.nasa.gov/rss/dyn/breaking_news.rss',
            'http://syndication.howstuffworks.com/rss/science']

        # content = urllib.request.urlopen(science_news[0]).read().decode('utf-8')
        content = urllib.request.urlopen(science_news[0]).read().decode('utf-8')
        my_all = self.get_rss_info_v1(content)



        self.topics.insert_many(my_all)

    def get_rss_info_v1(self, content):
        items = re.findall(r'<item>(.+?)</item>', content, flags=re.DOTALL)

        title_searcher = re.compile(r'<title>([^<]+)')
        link_searcher = re.compile(r'isPermaLink="true">([^<]+)')
        desc_searcher = re.compile(r'<description>([^<]+)')
        tumb_searcher = re.compile(r'<enclosure url="([^"]+)')

        final_items = []

        all_users = self.users.find()
        print(dir(all_users[2]))
        print(all_users[2].get('_id'))
        print(all_users[randint(0, 150)]['_id'])

        for i in items:
            my_dict = {
                'title': title_searcher.findall(i),
                'link': link_searcher.findall(i),
                'description': desc_searcher.findall(i),
                'thumbnail': tumb_searcher.findall(i),
            }
            final_items.append({
                'title': my_dict['title'][0] if len(my_dict['title']) > 0 else '',
                'link': my_dict['link'][0] if len(my_dict['link']) > 0 else '',
                'description': my_dict['description'][0] if len(my_dict['description']) > 0 else '',
                'thumbnail': my_dict['thumbnail'][0] if len(my_dict['thumbnail']) > 0 else '',
                'user': ObjectId(all_users[randint(0, 150)]['_id'])
            })

        return final_items



    @staticmethod
    def generate_users():
        max_users = 200
        lower_limit = 200

        user_api_url = 'https://svnweb.freebsd.org/csrg/share/dict/propernames?view=co&content-type=text/plain'
        content = urllib.request.urlopen(user_api_url).read().decode('utf-8')
        usernames = [user for user in content.split('\n') if len(user) > 5][:max_users]

        avatar_api_url = 'http://graph.facebook.com/v2.5/%d/picture?height=200&height=200&redirect=False'
        exclude_photo = 'https://static.xx.fbcdn.net/rsrc.php/v3/yo/r/UlIqmHJn-SK.gif'
        avatars_urls = [avatar_api_url % my_id for my_id in range(lower_limit, lower_limit + max_users + 1)]
        contents = [urllib.request.urlopen(i).read() for i in avatars_urls]
        avatars = [json.loads(content)['data']['url'] for content in contents]
        avatars = [av for av in avatars if av != exclude_photo]

        country_api_url = 'https://restcountries.eu/rest/v2/region/europe?fields=name'
        content = urllib.request.urlopen(country_api_url).read()
        countries = [obj['name'] for obj in json.loads(content)]

        gender = ['male', 'female']

        final_users = []

        lens = {
            'usernames': len(usernames),
            'avatars': len(avatars),
            'countries': len(countries),
            'gender': len(gender),
        }

        for _ in range(lens['usernames']):
            my_rand = randint(0, lens['usernames'])
            final_users.append({
                'user_name': usernames[my_rand],
                'user_url_avatar': avatars[my_rand % lens['avatars']],
                'user_country': countries[my_rand % lens['countries']],
                'user_gender': gender[my_rand % lens['gender']]
            })

        json.dump(final_users, open('dumps/users.dump', 'w+'), indent=4)

    def insert_threads(self):
        threads_titles = ['Science', 'Biology', 'History', 'Technology', 'Politics', 'Music', 'Movies']
        obj_to_add = [{'thread_name': title, 'topics': []} for title in threads_titles]

        self.threads.insert_many(obj_to_add)


if __name__ == '__main__':
    myrfa_dbm = MyrfaDBCreator()
    # myrfa_dbm.create_users_collection()
    myrfa_dbm.generate_topics()
    # # myrfa_dbm.insert_threads()
    pass