from Server.MServer import Server
from Models.ThreadModel import ThreadModel
from DBManager.db_connector import DBSession
import sys

if __name__ == '__main__':
    port = 8000
    if len(sys.argv) > 1:
        port = int(sys.argv[1])

    myrfa_server = Server(port=port)
    myrfa_server.start_server()

    # db_session = DBSession()
    # t = ThreadModel(db_session=db_session)
    #
    # # t.create_resource('Maria', [1,2,3])
    #
    # print(t.get_resource('Maria'))

