from myrfa.Models.BaseModel import BaseModel
import json


class TopicModel(BaseModel):

    def __init__(self, db_session=None):
        if db_session is None:
            raise Exception('No db Session recieved.')
        self.db_session = db_session

    def create_resource(self, user_name=None, user_gender=None, user_country=None, user_url_avatar=None):
        my_locals = locals()
        if None in my_locals.values():
            raise Exception(400, 'Some parameters are missing')

        found_user = self.db_session.users.find_one({'user_name': user_name})
        if found_user is None:
            user_to_insert = {
                key: my_locals[key] for key in my_locals
                if my_locals[key] is not None and key is not 'self'}

            inserted_user = self.db_session.users.insert_one(user_to_insert)
            found_user = self.db_session.users.find({'user_name': user_name}, {'_id': 0})

            return list(found_user)
        else:
            raise Exception(409, 'The User already exists')

    def delete_resource(self, user_name=None, user_gender=None, user_country=None, user_url_avatar=None):
        my_locals = locals()

        user_to_delete = {
            key: my_locals[key] for key in my_locals
            if my_locals[key] is not None and key is not 'self'}

        found_users = self.db_session.users.delete_many(user_to_delete)

        if found_users.deleted_count is 0:
            raise Exception(404, 'No users found.')

    def update_resource(self, user_name=None, user_gender=None, user_country=None, user_url_avatar=None):
        my_locals = locals()
        print(my_locals)
        if None in my_locals.values():
            raise Exception(400, 'Some parameters are missing')

        found_user = self.db_session.users.find_one({'user_name': user_name})
        if found_user is not None:
            data_to_replace = {
                key: my_locals[key] for key in my_locals
                if my_locals[key] is not None and key is not 'self'}

            inserted_user = self.db_session.users.find_one_and_replace({'user_name': user_name}, data_to_replace)
            found_user = self.db_session.users.find({'user_name': user_name}, {'_id': 0})

            return list(found_user)
        else:
            raise Exception(404, 'The User doesn\'t exists')

    def get_resource(self, description=None, link=None, title=None, thumbnail=None):
        my_locals = locals()
        topic_to_search = {
            key: my_locals[key] for key in my_locals
            if my_locals[key] is not None and key is not 'self'}

        found_topics = self.db_session.topics.find(topic_to_search, {'_id': 0, 'user': 0})
        #
        # for f in list(found_topics):
        #     # user_topic = dict(self.db_session.users.find_one({'_id': f['user']}, {'_id': 0}))
        #     # f['user'] = user_topic
        #     print('herter')

        # print(list(found_topics))

        if found_topics.count() is 0:
            raise Exception(404, 'No topic found.')
        return list(found_topics)
"""

{
    "_id" : ObjectId("58cb8c9ea52fe927ccce39bf"),
    "description" : "Simulations of superfluid helium show it follows the same unusual entropy rule that black holes do.",
    "link" : "https://www.sciencenews.org/article/superfluid-helium-behaves-black-holes",
    "title" : "Superfluid helium behaves like black holes",
    "thumbnail" : "http://www.sciencenews.org/sites/default/files/styles/620px_wide/public/2017/03/main/articles/031617_EC_entanglement_free.jpg?itok=q3CbJ38_",
    "user_id" : ObjectId("58cb08b9a52fe911105e7349")
}"""
