from abc import ABC, abstractmethod


class BaseModel(ABC):
    @abstractmethod
    def create_resource(self, **kwargs):
        pass

    @abstractmethod
    def get_resource(self, **kwargs):
        pass

    @abstractmethod
    def update_resource(self, **kwargs):
        pass

    @abstractmethod
    def delete_resource(self, **kwargs):
        pass
