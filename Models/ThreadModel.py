from .BaseModel import BaseModel


class ThreadModel(BaseModel):

    def __init__(self, db_session=None):

        if db_session is None:
            raise Exception('No db Session recieved.')

        self.db_session = db_session

    def create_resource(self, thread_name, topic_list=[]):
        if not isinstance(thread_name, str) or not isinstance(topic_list, list):
            raise Exception('Wrong parameters type')
        if len(thread_name) < 5:
            raise Exception('Thread name too short')

        final_obj = {
            'thread_name': thread_name,
            'topic_list': topic_list}
        found_one = self.db_session.threads.find_one({'thread_name': thread_name})

        if found_one is None:
            return self.db_session.threads.insert_one(final_obj)
        else:
            raise Exception('Resource already exists')

    def update_resource(self, thread_name, topic_list):
        if not isinstance(thread_name, str) or not isinstance(topic_list, list):
            raise Exception('Wrong parameters type')
        found_one = self.db_session.threads.find_one({'thread_name': thread_name})
        if found_one is None:
            raise Exception('The %s resource doesn\'t exists' % thread_name)

    def delete_resource(self, **kwargs):
        pass

    def get_resource(self, thread_name):
        if not isinstance(thread_name, str):
            raise Exception('Wrong parameter type')

        if thread_name is '':
            found_one = self.db_session.threads.find({}, {'_id': 0})
        else:
            found_one = self.db_session.threads.find_one({'thread_name': thread_name},{'_id': 0})

        if found_one is None:
            raise Exception('The %s resource doesn\'t exists' % thread_name)
        else:
            if isinstance(found_one, dict):
                return [found_one]
            else:
                return list(found_one)
