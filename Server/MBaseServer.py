from .MHTTPHandler import MyrfaHTTPHandler as HTTPHandler
import socketserver
import os


class BaseServer:
    def __init__(self, ip='127.0.0.1', port=80, working_dir=''):

        self.www_root = '../www'

        final_path = os.path.join(os.getcwd(), self.www_root, working_dir)
        os.chdir(final_path)

        self.http_handler = HTTPHandler
        self.httpd = socketserver.TCPServer(
            server_address=(ip, port),
            RequestHandlerClass=self.http_handler)

    def start_server(self):
        self.httpd.serve_forever()
