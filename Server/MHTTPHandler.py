from http.server import BaseHTTPRequestHandler, HTTPStatus
from Utils.ResourceManager import ReMan
import json
import os


class MyrfaHTTPHandler(BaseHTTPRequestHandler):

    re_man = ReMan()

    def do_GET(self):
        print(self.path)
        if self.path == '/':
            self.send_data(file_name='index.html')
            return

        if self.path.endswith(('.css', '.html')):
            self.send_data(file_name=self.path)

        if self.path.endswith(('.js')):
            self.send_data(file_name=self.path, file_type='javascript')

        url = self.path.split('/')
        if url[1] == 'rest':
            goodies = None
            try:
                goodies = self.re_man.get_me_goodies(''.join(self.path.split('rest')[1:]))
            except Exception as e:
                self.send_error(e.args[0], e.args[1])
            if goodies:
                self.send_data(my_bytes=json.dumps(goodies, indent=4).encode('utf-8'), file_type='json')

        elif url[1] == 'users':
            self.send_data(file_name='users.html')
            return

    def do_POST(self):
        url = self.path.split('/')
        if url[1] == 'rest':
            goodies = None
            try:
                data_string = self.rfile.read(int(self.headers['Content-Length']))
                goodies = self.re_man.get_me_goodies(''.join(self.path.split('rest')[1:]),
                                                     method='POST', payload=data_string.decode())
                self.set_ok_headers()
            except Exception as e:
                self.send_error(e.args[0], e.args[1])

            if goodies:
                self.send_data(my_bytes=json.dumps(goodies, indent=4).encode('utf-8'), file_type='json')

    def do_PUT(self):
        url = self.path.split('/')
        if url[1] == 'rest':
            goodies = None
            try:
                data_string = self.rfile.read(int(self.headers['Content-Length']))
                goodies = self.re_man.get_me_goodies(''.join(self.path.split('rest')[1:]),
                                                     method='PUT', payload=data_string.decode())
                self.set_ok_headers()
            except Exception as e:
                self.send_error(e.args[0], e.args[1])

            if goodies:
                self.send_data(my_bytes=json.dumps(goodies, indent=4).encode('utf-8'), file_type='json')

    def do_DELETE(self):
        url = self.path.split('/')
        if url[1] == 'rest':
            goodies = None
            try:
                data_string = self.rfile.read(int(self.headers['Content-Length']))
                goodies = self.re_man.get_me_goodies(''.join(self.path.split('rest')[1:]),
                                                     method='DELETE', payload=data_string.decode())
                self.set_ok_headers()
            except Exception as e:
                self.send_error(e.args[0], e.args[1])

            # if goodies:
            #     self.send_data(my_bytes=json.dumps(goodies, indent=4).encode('utf-8'), file_type='json')

    def send_data(self, file_name=None, my_bytes=None, file_type=None):
        bytes_to_send = ''
        file_path = ''
        if file_name is not None:
            if file_name.startswith('/'):
                file_name = file_name[1:]
            wd = os.getcwd()
            file_path = os.path.join(wd, file_name)

            if os.path.isfile(file_path):
                if file_name is not None:
                    bytes_to_send = open(file_path, 'rb').read()
            else:
                self.send_error(HTTPStatus.NOT_FOUND, "%s doesn't exists." % file_path)

        if my_bytes is not None:
            bytes_to_send = my_bytes

        self.set_ok_headers(file_path.split('.')[-1] if file_type is None else file_type)
        self.wfile.write(bytes_to_send)

    def set_ok_headers(self, file_type='html'):
        self.send_response(200)
        self.send_header('Content-type', 'text/%s' % file_type)
        self.end_headers()

